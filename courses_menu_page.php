<?php

function edudms_crs_menu_render() {

?>
<div class=wrap>
		<form method="post" action="options.php">
				<?php settings_fields( 'edudms_crs_options_page' ); ?>
				<?php submit_button(); ?>
				<?php do_settings_sections ( 'edudms_crs_simple_management_section' ); ?>
				<div class="edudms_setting_title">Simple Management:</div>
				<input type="radio" value="on" name="edudms_crs_simple_on_setting" <?php checked( 'on', get_option( 'edudms_crs_simple_on_setting' ), true )?>>On
				<input type="radio" value="off" name="edudms_crs_simple_on_setting" <?php checked( 'off', get_option( 'edudms_crs_simple_on_setting' ), true )?>>Off
				
				
				
				
				
				<?php submit_button(); ?>

			</form>
	</div>

<?php }



function edudms_crs_options() {
	add_settings_section(
		'edudms_crs_simple_management_section',
		'Simple Management',
		'edudms_crs_simple_management_section_callback_function',
		'edudms_crs_options_page'
	);


register_setting( 'edudms_crs_options_page', 'edudms_crs_simple_on_setting' );
register_setting( 'edudms_crs_options_page', 'edudms_crs_simple_setting' );

}
add_action( 'admin_init', 'edudms_crs_options' );




function edudms_crs_simple_management_section_callback_function() {
	echo '<div class="instructions1">These settings change what users see when they edit their Person Profile.</div>';
	echo '<div class="instructions1">Check each box to allow users to fill out that section in their Person Profile. This will also add this tab (if content is added) to their Profile Page (visible to site visitors).</div>';
}




















?>