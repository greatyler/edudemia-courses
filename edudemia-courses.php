<?php
/*
Plugin Name: Edudemia Courses Addon
Plugin URI: 
Description: Add course management to your DMS
Version: 2.0.1
Author: Tyler Pruitt
Author URI:

Credits:
This plugin's director of development and primary developer is Tyler Pruitt.

The following plugins were sourced or referrenced in this project:
Code from:



Dependencies or Referrences:





License

Edudemia is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
any later version.

Edudemia software is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.
 
See http://www.gnu.org/licenses/old_licenses/gpl_2.0.en.html.
*/

include(dirname( __FILE__ ) . '/courses_menu_page.php');


//Activation hooks
register_activation_hook( __FILE__, 'edudms_crs_activate' );

function edudms_pub_activate() {
	
}



// Function to create custom post type: courses
function edudms_crs_create_courses_posttype() {

	register_post_type( 'course',
	// CPT Options
		array(
			'labels' => array(
				'name' => __( 'Courses' ),
				'singular_name' => __( 'Course' )
			),
			'public' => true,
			'has_archive' => true,
			'description' => 'Courses in your department',
			'rewrite' => array('slug' => 'course'),
			'supports' => array( 'title' )
		)
	);
}

// Hooking up to theme setup
add_action( 'init', 'edudms_crs_create_courses_posttype' );

function edudms_crs_courses_submenu_settings() {
add_submenu_page('edit.php?post_type=course', 'edudms_crs_settings', 'Settings', 'manage_options', 'event_settings', 'edudms_crs_menu_render');
}
add_action("admin_menu", 'edudms_crs_courses_submenu_settings');






?>